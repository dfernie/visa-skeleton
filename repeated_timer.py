#!/usr/bin/env python
"""A module that provides the RepeatedTrigger class.

The RepeatedTrigger class is a threading timer that retriggers itself after 
each trigger, instead of being a one-shot timer like the standard library 
threading timer.
"""

__author__ = "David Fernie"
__copyright__ = "Copyright 2024 David Fernie"
__credits__ = ["David Fernie"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "David Fernie"
__email__ = "david.fernie@proton.me"
__status__ = "Production"

import threading
import time

class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer = None
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.is_running = False

    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self.next_call = time.time() + self.interval
            self._timer = threading.Timer(self.next_call - time.time(), self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False
