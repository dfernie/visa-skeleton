#!/usr/bin/env python
"""A barebones template application for PyVisa projects.

This application provides:
  - A basic TUI
  - Selection of the VISA intrument to connect to
  - Single key command interpretation
  - A basic command timer/scheduler
  - Printing of results to the terminal
  - Optional saving of the session to a log file
"""

__author__ = "David Fernie"
__copyright__ = "Copyright 2024 David Fernie"
__credits__ = ["David Fernie"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "David Fernie"
__email__ = "david.fernie@proton.me"
__status__ = "Development"

from repeated_timer import RepeatedTimer

import pyvisa
import readchar
import sys
import datetime


class Application():
    def __init__(self):
        """Returns a new Application object."""
        self.title = "PyVISA Skeleton Application"
        self.visa_rm = pyvisa.ResourceManager()
        self.instrument_address = ""
        self.instrument = None
        self.log_path = ""
        self.event_period = 1 # seconds
        self.event_timer = RepeatedTimer(self.event_period, self.instrument_event)

    def instrument_event(self):
        """The main function called by the event timer.
        
        This skeleton project has this function print the system time and the 
        instrument's response to an *IDN? query to STDIN in a CSV format.
        """
        line_str = "{0},{1}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), str(self.instrument.query("*IDN?")).strip())
        print(line_str)

    def run(self):
        """Starts the application interface and calls the main program loop."""
        print(self.title)
        self._select_instruments()
        application_stopped = False
        self.instrument_event()
        self.event_timer.start()
        while(not application_stopped):
            key = readchar.readkey()
            match key:
                case "q":
                    print("\nTerminating application...")
                    self.event_timer.stop()
                    sys.exit()
                case other:
                    print(key)

    def _list_instruments(self) -> tuple[str]:
        """Finds all VISA instruments connected and lists them."""
        instrument_list = self.visa_rm.list_resources()
        print("{0} instruments found. Enter number to select and instrument. Press the 'r' key to refresh the list. Press the 'q' key to quit.".format(len(instrument_list)))
        for i,instrument in enumerate(instrument_list):
            print("{0}.\t{1}".format(i+1, instrument))
        return instrument_list

    def _select_instruments(self):
        """Guides the user in selecting the instrument that will be used."""
        instrument_list = self._list_instruments()
        key_buffer = ""
        instrument_selected = False
        print("Selection: ", end="", flush=True)
        while(not instrument_selected):
            key = readchar.readkey()
            match key:
                case "q":
                    print("\nTerminating application...")
                    sys.exit()
                case "r":
                    instrument_list = self._list_instruments()
                case readchar.key.ENTER:
                    try:
                        self.instrument_address = instrument_list[int(key_buffer) - 1]
                        self.instrument = self.visa_rm.open_resource(self.instrument_address)
                        print("\n{0} was selected".format(self.instrument_address))
                        instrument_selected = True
                    except ValueError:
                        print("Not a number. Please try again.")
                        print("Selection: ", end="", flush=True)
                    except IndexError:
                        print("Invalid selection. Please try again.")
                        print("Selection: ", end="", flush=True)
                    finally:
                        key_buffer = ""
                case readchar.key.BACKSPACE:
                    key_buffer = key_buffer[:-1]
                case other:
                    if key >= "0" and key < "9":
                        key_buffer += key
                        print(key, end="", flush=True)


if __name__ == '__main__':
    app = Application()
    app.run()